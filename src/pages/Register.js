import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import {Form, Button, Container, Row, Col} from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import googleIcon from "../icons/google.png";
import fbIcon from "../icons/fb.png";


export default function Register(){
    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [uName, setUName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(uName);
    console.log(email);
    console.log(password1);
    console.log(password2);

    useEffect(() =>{


        if((uName !== '' && email !== '' && password1 !== '' && password2 !=='') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [uName, email, password1, password2])

    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            }
            else{

                fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        username: uName,
                        email: email,
                        password: password1,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        });
                        setUName('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');
                        navigate("/login");
                    }
                    else{

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });

                    }
                })


            }
        })
    }

    return(

        <Container className="main-container reg-container" fluid>
            <Row className="reg-row">
                <Col md={8} lg={6}  className="reg-img-col p-0">
                                           
                </Col>
                <Col md={4} lg={6}  className="form-col">
                    <Row>
                        <h2 className="mt-5 mb-3 ff-bold text-center">REGISTER NOW!</h2>
                    </Row>
                    <Row >
                        <span className="text-center py-3">Sign up using your social media accounts</span>
                    </Row >
                    <Row >
                        <Col md={12} className=" h-10 d-flex justify-content-center py-3">
                             <img src={googleIcon} className="p-1"/>
                             <img src={fbIcon} className="p-1"/>   
                        </Col>
                    </Row>
                    <Row className="text-center">
                        <span> ⎯⎯⎯⎯⎯ or ⎯⎯⎯⎯⎯ </span>
                    </Row>
                    <Row className=" px-5">
                        <Col className=" mx-auto px-5 reg-col" >
                            <Form onSubmit={e => registerUser(e)}>

                            <Form.Group className="mb-3" controlId="userName">
                                <Form.Label>Username</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter first name"
                                    value={uName}
                                    onChange={e => setUName(e.target.value)}
                                    required
                                />
                            </Form.Group>


                            <Form.Group className="mb-3" controlId="emailAddress">
                                <Form.Label>Email Address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter email"
                                    onChange={e => setEmail(e.target.value)}
                                    value={email}
                                    required
                                />
                                <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>


                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password" 
                                    placeholder="Enter Password"
                                    value={password1}
                                    onChange={e => setPassword1(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password2">
                                <Form.Label>Verify Password</Form.Label>
                                <Form.Control
                                    type="password" 
                                    placeholder="Verify Password"
                                    value={password2}
                                    onChange={e => setPassword2(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            {
                                isActive
                                ?
                                    <Button variant="dark" type="submit" id="submitBtn">
                                    Submit
                                    </Button>
                                :
                                    <Button variant="light" type="submit" id="submitBtn" disabled>
                                    Submit
                                    </Button>
                            }
                            </Form>
                       </Col>
                    </Row>
                </Col>
             </Row>
        </Container>
    )
}
