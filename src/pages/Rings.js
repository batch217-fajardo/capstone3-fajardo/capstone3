import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import bannerR from "../images/bannerR.png"
import {Container, Row, Col } from 'react-bootstrap';


export default function Rings() {


	const [rings, setRings] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/rings`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setRings(data.map(ring =>{
				return(
					<ProductCard key={ring._id} productProp={ring}/>
				);
			}));
		})
	}, []);

	return(

		<>
		<Row>
		<img src={bannerR}/>
		</Row>
		<Container className="main-container ">
		<Row><h1 className="text-center ff-bold p-1">Rings</h1></Row>
		<Row className="products-row p-1">
		  <Col xs={12} md={6} lg={4} className="p-1">
			{rings[0]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{rings[1]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{rings[2]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{rings[3]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{rings[4]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{rings[5]}
		  </Col >
		 </Row>
		</Container>
		</>
	)
}