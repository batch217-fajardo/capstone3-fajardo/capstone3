
import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";

export default function Error(){
    return(

        <Row className="main-container">
            <Col className="p-5 text-center">
                <h1 className="ff-bold" >404 - Page Not Found!</h1>
                <p>The page you are trying to access cannot be found.</p>
                <Button variant="dark" as = {Link} to="/">Back to Store</Button>
            </Col>
        </Row>
        
    )
    
}