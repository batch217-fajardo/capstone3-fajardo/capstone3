import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";

import {Container, Row, Col } from 'react-bootstrap';
import bannerN from "../images/bannerN.png"


export default function Necklaces() {

	const [necklaces, setNecklaces] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/necklaces`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setNecklaces(data.map(necklace =>{
				return(
					<ProductCard key={necklace._id} productProp={necklace}/>
				);
			}));
		})
	}, []);

	return(
		<>
		<Row>
		<img src={bannerN}/>
		</Row>
		<Container className="main-container ">
		
		<Row><h1 className="text-center ff-bold p-1">NECKLACES</h1></Row>
		<Row className="products-row p-1">
		  <Col xs={12} md={6} lg={4} className="p-1">
			{necklaces[0]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{necklaces[1]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{necklaces[2]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{necklaces[3]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{necklaces[4]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{necklaces[5]}
		  </Col >
		 </Row>
		</Container>
		</>
	)
}