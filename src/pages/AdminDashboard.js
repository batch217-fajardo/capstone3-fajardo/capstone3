import React from "react"
import { Link } from "react-router-dom";
import { Container, Row, Col, Button, Stack } from "react-bootstrap";
 import Sidebar from "../components/SidebarMenu.js";
 import AllProducts from "./AllProducts.js";
 import AllOrders from "./AllOrders.js";
 import placeholder1 from "../images/placeholder1.png";
 import placeholder2 from "../images/placeholder2.png";
 import placeholder3 from "../images/placeholder3.png";



export default function AdminDashboard(){


	return(
		 <>
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
        <Col className="bg-white d-flex  pt-3 m-0 shadow" xs={12} md={2} lg={2}>
        <Sidebar />
        </Col>

        <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={10}>

        <>   

        <div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5 dashboard-container">
            <div className="my-2 text-center">
                <h1 className="ff-bold">Welcome back, Admin!</h1>
            </div>
                <Container className="">
                <Row className="all-orders-row">
                	<Col className="empty square m-1">
                	</Col> 
                	<Col className="empty square m-1">
                	</Col>
                	<Col className="empty square m-1">
                	</Col>
                </Row>
                <Row>
                	<Col className="empty rectangle m-1">
                	</Col>
                </Row>
                </Container>
        </div>
        </>

        </Col>
        
        </Row>
    </Container>
    </>




	

	)
}