import React from "react"
import { Link } from "react-router-dom";
import { Container, Row, Col, Button, Stack } from "react-bootstrap";
import Hero from "../components/Hero.js";
import necklaceImg from "../images/necklace.jpg"
import ringImg from "../images/rings.jpg"
import earringImg from "../images/earring.jpg"
import truckIcon from "../icons/truck.png"
import certificateIcon from "../icons/certificate.png"
import guaranteeIcon from "../icons/guarantee.png"


export default function Home(){



	return(
		<>
		<Hero />
		

	{/*categories*/}
		<Container className="cat-container my-5 pb-5">
			<Row className="text-center">
			<h2 className="ff-bold pt-5"> SHOP BY CATEGORY</h2>
			</Row>
			<Row>
				<Col xs={12} lg={4}>
				<img src={necklaceImg} className="p-5"/>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/necklaces"} variant="dark" size="sm">Necklace</Button>
				</Stack>
				</Col>
					

				<Col xs={12} lg={4}>
				<img src={ringImg} className="p-5"/>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/rings"} variant="dark" size="sm">Ring</Button>
				</Stack>
				</Col>	

				<Col xs={12} lg={4}>
				<img src={earringImg} className="p-5"/>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/earrings"} variant="dark" size="sm">Earring</Button>
				</Stack>
				</Col>

			</Row>
		</Container>

	{/*promos*/}
	<Container className="promo-container text-center" fluid>
		<Row>
			<Col xs={12} md={4} className="promo-icon-col p-3">
				<Stack className="d-flex align-items-center p-2">
				<img src={truckIcon} />
				<h5 className="p-1 ff-bold">LOREM IPSUM</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</Stack>

			</Col>
			<Col xs={12} md={4} className="promo-icon-col p-3">
				<Stack className="d-flex align-items-center p-2">
				<img src={certificateIcon} />
				<h5 className="p-1 ff-bold">LOREM IPSUM</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</Stack>
			</Col>
			<Col xs={12} md={4} className="promo-icon-col p-3">
				<Stack className="d-flex align-items-center p-2">
				<img src={guaranteeIcon}/>
				<h5 className="p-1 ff-bold">LOREM IPSUM</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</Stack>
			</Col>
		</Row>
	</Container>

	{/*sale*/}

	<Container className="main-container sale-container">
		<Row>
			<Col xs={12} md={6} className="sale-txt">
				<Stack>
				 <h1 className="sale-title ff-bold">Lorem ipsum dolor sit amet,</h1>	
				 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum sollicitudin sem sed tempor. Praesent nec urna tortor. Phasellus semper ex finibus accumsan euismod. Sed sollicitudin ligula augue, sed luctus odio facilisis vitae. Praesent tincidunt ullamcorper egestas. Quisque elit augue, feugiat gravida nunc facilisis, volutpat aliquet nunc. Quisque molestie libero blandit dolor rutrum, sit amet vestibulum sem fringilla. In id erat sollicitudin lacus eleifend sagittis. Integer arcu magna, tempus et odio quis, bibendum eleifend mauris. Donec eget ante et magna convallis egestas at sed lorem. Sed congue volutpat gravida. Etiam cursus sapien quis tortor elementum egestas. Donec luctus eleifend congue. Morbi nec semper dui, vulputate blandit mi. Cras pretium lobortis nulla ut tristique. Sed porttitor sem in justo fringilla ornare. Curabitur vestibulum finibus dui, eget commodo nibh molestie a. Donec lobortis sagittis nulla ut vestibulum. Suspendisse ullamcorper lobortis eleifend. Duis ultricies turpis id neque imperdiet porta. Proin gravida eu tellus a rutrum. Curabitur fringilla suscipit sem id pharetra. Nullam accumsan metus id enim vestibulum euismod.
				 </p>
				</Stack>
			</Col>
			<Col className="sale-img">

			</Col>
		</Row>
	</Container>

{/*contact us*/}

	<Container className="contact-container" fluid>
		<Row className="py-3">
			<Col className="ff-bold"> Contact us through: </Col>
		</Row>
		<Row className="ff-reg">
			<Col xs={12} md={4} >
			<Stack>
			<a href="https://www.facebook.com/">Facebook</a>
			<a href="https://www.instagram.com/">Instagram </a>
			<a href="https://twitter.com/">Twitter</a>
			</Stack>
			</Col>
			<Col xs={12} md={4} >
			<p>loremipsum@mail.com</p>
			<p> +639 123 4567 </p>
			</Col>
			<Col xs={12} md={4} >
			<p>Visit Our Store!</p>
			<p> #123 Somewhere St. That City, This Province </p>
			</Col>
		</Row>
	</Container>

		</> 

	

	)
}