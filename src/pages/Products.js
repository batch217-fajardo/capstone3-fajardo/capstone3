import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import bannerP from "../images/bannerP.png"

import {Container, Row, Col } from 'react-bootstrap';


export default function Products() {


	const [products, setProducts] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);
	return(

				<>
		<Row>
		<img src={bannerP}/>
		</Row>
		<Container className="main-container p-5">
		<Row><h1 className="text-center ff-bold p-1">Products</h1></Row>
		<Row className="products-row p-5">
		  <Col xs={12} lg={4} className="p-1">
			{products[0]}
		  </Col >
		  <Col  xs={12} lg={4} className="p-1">
			{products[1]}
		  </Col>
		  <Col  xs={12} lg={4} className="p-1">
			{products[2]}
		  </Col>
		   <Col  xs={12}  lg={4} className="p-1">
			{products[3]}
		  </Col>
		  <Col  xs={12} lg={4} className="p-1">
			{products[4]}
		  </Col>
		  <Col  xs={12} lg={4} className="p-1">
			{products[5]}
		  </Col>		  
		  <Col  xs={12} lg={4} className="p-1">
			{products[6]}
		  </Col>		  
		  <Col  xs={12} lg={4} className="p-1">
			{products[7]}
		  </Col>		  
		  <Col  xs={12} lg={4} className="p-1">
			{products[8]}
		  </Col>
		  <Col  xs={12} lg={4} className="p-1">
			{products[9]}
		  </Col>
		  <Col  xs={12} lg={4} className="p-1">
			{products[10]}
		  </Col>		  
		  <Col  xs={12} lg={4} className="p-1">
			{products[11]}
		  </Col>
		 </Row>
		</Container>
		</>
	)
}