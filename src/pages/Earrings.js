import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import bannerE from "../images/bannerE.png"

import {Container, Row, Col } from 'react-bootstrap';


export default function Earrings() {


	const [earrings, setEarrings] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/earrings`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setEarrings(data.map(earring =>{
				return(
					<ProductCard key={earring._id} productProp={earring}/>
				);
			}));
		})
	}, []);

	return(
		<>
		<Row>
		<img src={bannerE}/>
		</Row>
		<Container className="main-container ">
		<Row><h1 className="text-center ff-bold p-1">Earrings</h1></Row>
		<Row className="products-row p-1">
		  <Col xs={12} md={6} lg={4} className="p-1">
			{earrings[0]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{earrings[1]}
		  </Col >		  
		  <Col xs={12} md={6} lg={4} className="p-1">
			{earrings[2]}
		  </Col >
		  <Col xs={12} md={6} lg={4} className="p-1">
			{earrings[3]}
		  </Col >
		  <Col xs={12} md={6} lg={4} className="p-1">
			{earrings[4]}
		  </Col >
		  <Col xs={12} md={6} lg={4} className="p-1">
			{earrings[5]}
		  </Col >
		 </Row>
		</Container>
		</>
	)
}