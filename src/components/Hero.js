import React from "react"
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";

export default function Hero(){


	return(

		<Container fluid className="hero-container">
			<Row className="hero-row align-items-center">
				<Col></Col>
				<Col></Col>
				<Col xs={12} lg={4} className="px-5">
					<Row >
					<h1 className="ff-bold">Timeless Jewelry Pieces</h1>
					</Row>
					<Row>
					<p className="ff-reg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu dui non neque tempus faucibus in quis urna. Donec eget consequat ex.</p>
					</Row>
					<Row className="pt-3">
					<Col xs={12} lg={6}>
						<Button  as={Link} to={"/products"} variant="dark" size="sm">SHOP NOW</Button>
					</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}
