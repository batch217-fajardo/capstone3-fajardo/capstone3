import SidebarMenu from 'react-bootstrap-sidebar-menu';
import dashIcon from '../images/dashboard.png' 
import {Container, Row, Col } from 'react-bootstrap'
import { NavLink } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";


export default function Sidebar(){

  const { user } = useContext(UserContext);
    console.log(user);

    const [activeLink, setActiveLink] = useState('dashboard');
  


  const onUpdateActiveLink = (value) => {
    setActiveLink(value);
  }  

  return (



<SidebarMenu>

  <SidebarMenu.Header>
  <SidebarMenu.Nav.Link as={ NavLink } to="/dashboard" end className={activeLink === '/dashboard' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('dashboard')} >
    <SidebarMenu.Brand>
     <h5>DASHBOARD</h5>
    </SidebarMenu.Brand>
    </SidebarMenu.Nav.Link>
  </SidebarMenu.Header>

  <SidebarMenu.Body>

        <SidebarMenu.Nav>
          <SidebarMenu.Nav.Link as={ NavLink } to="/dashboard/products" end className={activeLink === '/dashboard/products' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('products')}>
            <SidebarMenu.Nav.Title>
                <p>All Products</p>
            </SidebarMenu.Nav.Title>
          </SidebarMenu.Nav.Link>
        </SidebarMenu.Nav>

        <SidebarMenu.Nav>
          <SidebarMenu.Nav.Link as={ NavLink } to="/dashboard/orders" end className={activeLink === '/dashboard/orders' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('orders')}>
            <SidebarMenu.Nav.Title>
                <p>All Orders</p>
            </SidebarMenu.Nav.Title>
          </SidebarMenu.Nav.Link>
        </SidebarMenu.Nav>  
  </SidebarMenu.Body>

</SidebarMenu>

    )
}
